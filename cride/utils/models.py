'''Django models utilites.'''

# Django
from django.db import models

class CRideModel(models.Model):
    '''Comparte Ride base.model

    CrideModel acts as an abstract base class from which every other
    model in the project will inherit. This class provides every table
    with the following attributes:
        + created (datetime): Store the datetime the object was created.
        + modified (datetime): Store the datetime the object was modified.
    '''

    created = models.DateTimeField(
        'created at',
        auto_now_add=True,
        help_text='Date time which the object was created'
        )
    modified =  models.DateTimeField(
        'modified at',
        auto_now=True,
        help_text='Date time which the object was last modified'
        )

    class Meta:
        ''' Meta options'''
        abstract = True
        get_latest_by = 'created'
        ordering = ['-created', '-modified']

# class Student(CRideModel):
#     '''Student class proxy model
#         Student acts as an proxy model class, this kind of class is used
#         for applied several extra functions in a model
#     '''
#     name = models.CharField()

#     class Meta(CRideModel.META):
#         '''
#             Student meta inherited attributes from CrideModel.Meta, here just
#             add the db_table name for Studen model
#         '''
#         db_table = 'student_role'

# # Using Proxy models for add atributes in a model
# class Entity(models.model):
#     dni = models.CharField()
#     ruc = models.CharField()

# class Person(Entity)
#     class Meta:
#         proxy = True

#     def get_document(self, ):
#         return self.dni

# class Enterprise(Entity)
#     class Meta:
#         proxy = True

#     def get_document(self, ):
#         return self.ruc
