'''Users Serializers'''

# python
from datetime import timedelta
import jwt

# Django
from django.contrib.auth import authenticate, password_validation
from django.utils import timezone

# djangorestframework
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.validators import UniqueValidator

# django
from django.core.validators import RegexValidator
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.conf import settings

# models
from cride.users.models import User, Profile


class UserModelSerializer(serializers.ModelSerializer):
    '''User model serializer'''

    class Meta:
        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
            'phone_number'
        )


class UserLoginSerializer(serializers.Serializer):
    '''User Login Serializer.
        Handle the login request
    '''

    email = serializers.EmailField()
    password = serializers.CharField(min_length=0, max_length=64)

    def validate(self, data):
        '''Check credentials.'''

        user = authenticate(username=data['email'], password=data['password'])

        if not user:
            raise serializers.ValidationError('Invalid credentials')
        if not user.is_verified:
            raise serializers.ValidationError('Account is not active yet!')
        self.context['user'] = user
        return data

    def create(self, data):
        '''Generate o Retrieve new token'''
        token, user = Token.objects.get_or_create(user=self.context['user'])
        return user, token.key


class UserSignUpSerializer(serializers.Serializer):
    '''
        User Signup serializer
        Register a new user
    '''
    email = serializers.EmailField(

    )
    username = serializers.CharField(min_length=0,
                                     max_length=64,
                                     validators=[UniqueValidator(queryset=User.objects.all())]
                                     )

    # name
    first_name = serializers.CharField(min_length=2, max_length=64)
    last_name = serializers.CharField(min_length=2, max_length=64)

    # password
    password = serializers.CharField(min_length=0, max_length=64)
    password_confirmation = serializers.CharField(min_length=0, max_length=64)

    # phone_number
    phone_regex = RegexValidator(
        regex=r'\+?1?\d{9,15}$',
        message='Phone number must be between 1 and 15 characters long '
    )
    phone_number = serializers.CharField(min_length=9,
                                         max_length=17,
                                         validators=[phone_regex])

    def validate(self, data):
        '''Check data.'''

        passwd = data['password']
        passwd_conf = data['password_confirmation']

        if passwd != passwd_conf:

            error = {
                "password_confirmation": [
                    "Passwords don't match."
                ]
            }

            raise serializers.ValidationError(error)

        password_validation.validate_password(passwd)

        return data

    def create(self, data):
        ''' Handle user and profile creation. '''
        data.pop('password_confirmation')
        user = User.objects.create_user(**data, is_verified=False)
        profile = Profile.objects.create(user=user)
        self.send_confirmation_email(user)
        return user

    def send_confirmation_email(self, user):
        '''send account verification link to given user'''
        verification_token = self.gen_verification_token(user)

        subject = 'welcome @{}| verify your account to start using comparte ride'.format(user.username)
        from_email = 'Comparte Ride <noreply@comparteride.com>'
        content = render_to_string(
            'emails/users/account_verification.html',
            {'token': verification_token, 'user': user}
            )
        msg = EmailMultiAlternatives(subject, content, from_email, [user.email])
        msg.attach_alternative(content, "text/html")
        msg.send()

    def gen_verification_token(self, user):
        '''Create a token jwt that the user can use to verify its account'''
        exp_date = timezone.now() + timedelta(days=3)
        payload = {
            'user': user.username,
            'exp': int(exp_date.timestamp()),
            'type': 'email_confirmation'
        }

        token = jwt.encode(payload, settings.SECRET_KEY, algorithm='HS256')
        return token

class AccountVerificationSerializer(serializers.Serializer):
    '''Account Verification Serializer'''

    token = serializers.CharField()

    def validate_token(self, data):
        '''Verify token is valid'''

        try:
            payload = jwt.decode(data, settings.SECRET_KEY, algorithm='HS2560')
        except jwt.ExpiredSignatureError:
            raise serializers.ValidationError('Verification link has expired')
        except jwt.PyJWTError:
            raise serializers.ValidationError('invalid token')

        if payload['type'] != 'email_confirmation':
            raise serializers.ValidationError('Invalid Token')

        self.context['payload'] = payload
        return data

    def save(self):
        '''Update user's verify status'''

        payload = self.context['payload']
        user = User.objects.get(username=payload['user'])
        user.is_verified = True
        user.save()
