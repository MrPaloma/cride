""" Circles Views """

# Django REST Framework
from rest_framework.decorators import api_view
from rest_framework.response import Response

# models
from cride.circles.models import Circle

# serializers
from cride.circles.serializers import (CircleSerializer, CreateCircleSerializer)

# from rest_framework.decorators import throttle_classes
# from rest_framework.throttling import AnonRateThrottle

# class OncePerDayThrottle(AnonRateThrottle):
#     rate = '5/day'

@api_view(['GET'])
# @throttle_classes([OncePerDayThrottle])
def list_circles(request):
    """ List Circles """
    circles = Circle.objects.filter(is_public=True)
    serializer = CircleSerializer(circles, many=True)
    return Response(serializer.data)


@api_view(['POST'])
def create_circle(request):
    """Create a circle"""
    serializer = CreateCircleSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    circle = serializer.save()
    return Response(CircleSerializer(circle).data)
