""" Circles Model """

# Django
from django.db import models

# Utils
from cride.utils.models import CRideModel


class Circle(CRideModel):
    """ Circles Model

        A circle is a private group where rides are offered an taken by its members.
    To join a circle a user must retrieve an unique invitation code from an existing
    circle member
    """

    name = models.CharField("circle name", max_length=140)
    slug_name = models.SlugField(unique=True, max_length=140)

    about = models.CharField('circle description', max_length=255)
    picture = models.ImageField(upload_to='circles/pictures', blank=True, null=True)

    # Stats
    rides_offered = models.PositiveIntegerField(default=0)
    rides_taken = models.PositiveIntegerField(default=0)

    members = models.ManyToManyField(
        'users.User',
        through='circles.Membership',
        through_fields=('circle', 'user')
    )

    verified = models.BooleanField(
        'verified circle',
        default=False,
        help_text='verified circles are also know as official communities.'
    )

    is_public = models.BooleanField(
        default=True,
        help_text='public circles are listed in the main page so everyone know about their existence.'
    )

    is_limited = models.BooleanField(
        'limited',
        default=True,
        help_text='limited circles can grow up to a fixed number of members.'
    )

    members_limit = models.PositiveIntegerField(
        default=0,
        help_text='If a circle is limited, this will be limited on the number of members.'
    )

    def __str__(self):
        """Returns circle name"""
        return self.name

    class Meta(CRideModel.Meta):
        """ Neta Class """
        ordering = ('-rides_taken', '-rides_offered')
