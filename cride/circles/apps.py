''' Circle App '''

# Django
from django.apps import AppConfig

class CirclesAppConfig(AppConfig):
    ''' Circle App config '''
    name = 'cride.circles'
    verbose_name = 'Circles'
