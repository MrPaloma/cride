""" Circle Model admin """

# Django Imports
from django.contrib import admin

from cride.circles.models import Circle

@admin.register(Circle)
class CircleAdmin(admin.ModelAdmin):
    """ Circle Model admin """
    list_display = ('slug_name', 'name', 'is_public', 'verified', 'is_limited', 'members_limit')
    list_filter = ('is_public', 'is_limited', 'verified')
    search_fields = ('slug_name', 'name')
